//! https://developer.android.com/ndk/reference/group/logging

//! FIXME: WARNING -- these functions fail to generate log messages properly
//! but instead produce entries with empty strings.  Contributors welcomed!

use std::ffi::CString;
use std::os::raw::{c_int, c_char};

#[repr(C)]
#[allow(non_camel_case_types)]
#[allow(dead_code)]
enum LogPriority {
    ANDROID_LOG_UNKNOWN = 0,
    ANDROID_LOG_DEFAULT,
    ANDROID_LOG_VERBOSE,
    ANDROID_LOG_DEBUG,
    ANDROID_LOG_INFO,
    ANDROID_LOG_WARN,
    ANDROID_LOG_ERROR,
    ANDROID_LOG_FATAL,
    ANDROID_LOG_SILENT
}

const TAG: &str = "anagrams";

// See ${ANDROID_NDK}/*/sysroot/usr/include/android/log.h
// which should be generated via
// ${ANDROID_SDK}/ndk-bundle/build/tools/make_standalone_toolchain.py
// See [other Makefile](../../android-app/Makefile)
extern {
    fn __android_log_write(prio: c_int, tag: *const c_char,
                           text: *const c_char) -> c_int;
}

#[allow(dead_code)]
pub fn log_debug(text: String) {
    let tag = CString::new(TAG).unwrap().as_ptr();
    let text = CString::new(text).unwrap().as_ptr();
    unsafe {
        __android_log_write(LogPriority::ANDROID_LOG_DEBUG as c_int, tag, text);
    }
}

#[allow(dead_code)]
pub fn log_warn(text: String) {
    let tag = CString::new(TAG).unwrap().as_ptr();
    let text = CString::new(text).unwrap().as_ptr();
    unsafe {
        __android_log_write(LogPriority::ANDROID_LOG_WARN as c_int, tag, text);
    }
}

#[allow(dead_code)]
pub fn log_error(text: String) {
    let tag = CString::new(TAG).unwrap().as_ptr();
    let text = CString::new(text).unwrap().as_ptr();
    unsafe {
        __android_log_write(LogPriority::ANDROID_LOG_ERROR as c_int, tag, text);
    }
}

#[allow(dead_code)]
pub fn log_fatal(text: String) {
    let tag = CString::new(TAG).unwrap().as_ptr();
    let text = CString::new(text).unwrap().as_ptr();
    unsafe {
        __android_log_write(LogPriority::ANDROID_LOG_FATAL as c_int, tag, text);
    }
}
