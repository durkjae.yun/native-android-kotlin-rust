#! /bin/bash

# This script is necessary because `clang` lacks conventional use
# of `ld` related environment variables or anything equivalent:
# export LDFLAGS="--hash-style=sysv"
# https://clang.llvm.org/docs/CommandGuide/clang.html#environment

# ${ANDROID_NDK} contains generated standalone tools rather than
# what ships with Android SDK, and this may be achieved by running:
# ${ANDROID_SDK}/ndk-bundle/build/tools/make_standalone_toolchain.py \
#		--force --api ${ANDROID_VERSION} \
#		--arch arm --install-dir ${ANDROID_NDK}/arm;

${ANDROID_NDK}/arm/bin/arm-linux-androideabi-clang -Wl,--hash-style=both $*
