Android Rust
============

This library bridges the
[anagram-phrases](https://github.com/dpezely/anagram-phrases) library
written in Rust and [android-app](../android-app/) for creating a pure
Android Native app.

The general approach comes from the
[Visly blog](https://medium.com/visly/rust-on-android-19f34a2fb43).

See [Makefile](./Makefile) for compiling this library and
[android-app's Makefile](../android-app/Makefile) for linking
via the Android Native Development Kit (NDK).


## Setup

This requires the Rust compiler, and version 1.42 was used during principal
development.  Future releases should work without modification.

Run:

    make linux-deps deps
    

## Compile

Run:

    make build


## Android Hardware Architecture Support

See [Rust programming language](https://www.rust-lang.org/)'s
[platform support](https://forge.rust-lang.org/release/platform-support.html)
page for current list of supported hardware.

Practical CPU architectures used by Android mobile devices as of 2020:

- 64-bit ARM64 Android: `aarch64-linux-android`
- 32-bit ARMv7a Android: `armv7-linux-androideabi`
- 32-bit x86 Android: `i686-linux-android`
- 64-bit x86 Android: `x86_64-linux-android`

Unsupported by the Android NDK toolchain:

- Thumb2-mode ARMv7a Android with NEON: `thumbv7neon-linux-androideabi`

References to "`eabi`" indicate *emulated* application binary interface
(ABI), because the ARM v7 instruction set has variants such as v7-A and NEON
extensions.


## Dynamic Linking

Using the Android SDK, a standalone toolchain of `clang` performs linking to
generate the shared dynamic library of the `.so` file.

This introduces challenges for running on version older than Android 6.0.

The hash table from which the Android runtime would resolve function names
within the library may be `GNU_HASH` or older `HASH`, usually referred to as
"SYSV".

LLVM's Clang and GCC are capable of producing both hash tables, but `clang`
apparently defaults to `GNU_HASH`.

Using only `GNU_HASH` works [fine for Android 6.0 and higher](https://github.com/aosp-mirror/platform_bionic/blob/master/android-changes-for-ndk-developers.md#gnu-hashes-availible-in-api-level--23).

Changing this to use both GNU and SYSV hash for accommodating older devices
requires setting
[`clang` option](https://clang.llvm.org/docs/CommandGuide/clang.html)
`-Wl,--hash-style=both` which necessitates creating a shell script.

While other tools pass `LDFLAGS` to the linker (per Makefile conventions),
[`clang` environment variables](https://clang.llvm.org/docs/CommandGuide/clang.html#environment)
omit supporting such use cases.

See shell scripts within [bin](./bin/) subdirectory, and
[Makefile](./Makefile) specifies names of these scripts via Rust's
[`cargo` environment
variables](https://doc.rust-lang.org/cargo/reference/environment-variables.html#configuration-environment-variables).
An architecture "triple" embedded within the name of a Unix/POSIX
environment variable requires changing hyphens to underscores and using
SCREAMING_CASE, as demonstrated by our `make build` target.

(If not using Makefiles, Rust's `cargo` accommodates naming these shell
scripts within `~/.cargo/config` file, but this impacts *all* builds for the
specified architecture triple, not just this crate.)

The resulting `.so` library files may be confirmed to contain *both* hash
styles.

On Linux laptop/workstation:

    make build
    cd target/i686-linux-android/debug/
    readelf -d librust.so | grep HASH

> 0x6ffffef5 (GNU_HASH)                   0x10f8
> 0x00000004 (HASH)                       0x1128

When the output differs from the above, expect to see Android log messages
indicating and error with "linker" regarding "unused DT entry".

Finally, be sure to build the Android Native app such that
[`targetSdkVersion` matches `minSdkVersion`](https://github.com/aosp-mirror/platform_bionic/blob/master/android-changes-for-ndk-developers.md#__register_atfork-available-in-api-level--23).


## Addendum

Highlights from the
[Rust subreddit](https://www.reddit.com/r/rust/comments/fywgwj/native_android_app_with_kotlin_and_rust/)
discussion:

One alternative to the JNI crate used here would be [rust_swig](https://dushistov.github.io/rust_swig/).
See the [Android example](https://github.com/Dushistov/rust_swig/tree/master/android-example)


## License

[MIT License](../LICENSE)

Essentially, do as you wish and without warranty from authors or maintainers.
