package com.example.anagrams

import android.app.Application 
import android.content.Context
import android.content.res.AssetManager
import android.content.res.Resources
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import java.io.FileDescriptor

class ResultsViewModel: AndroidViewModel {

    private var queryKeywords: String = ""

    private var anagrams: Resultset =
        listOf("ERROR:",
               "Dictionary word-list file not found")

    constructor(application: Application):  super(application)

    // Loads text file containing wordlist, searches for matching anagrams
    // and stores results via `anagrams` private member field.
    // See file: app/src/main/assets/wordlist.txt
    public fun computeAnagrams(q: String): Resultset {
        if (library_loaded) {
            // Without the bogus `.png` file suffix, it would crash
            // here with java.io.FileNotFoundException:
            // "This file can not be opened as a file descriptor; it is
            // probably compressed"
            val fd_obj = getApplication<Application>().getApplicationContext()
                .getAssets().openFd("wordlist.txt.png").getFileDescriptor()
            // Details of the work-around:
            // https://groups.google.com/forum/#!topic/android-ndk/H4P0SAkzMsM
            // Thus:
            // val afd: AssetFileDescriptor? =
            //     getApplication<Application>().getApplicationContext()
            //     .getResources().openRawResourceFd(R.string.wordlist_txt)
            // afd?.let {
            //     val fd_obj: FileDescriptor = it.getFileDescriptor()
            //     val offset = it.getStartOffset()
            //     val length = it.getLength()
            queryKeywords = q
            anagrams = query(fd_obj as Any, q)
            if (BuildConfig.DEBUG) {
                Log.d("anagrams", "ResultsViewModel count=${anagrams.size}")
                for (phrase in anagrams) {
                    Log.d("anagrams", " Entry: ${phrase}")
                }
            }
        } else {
            // FIXME: what is the Android idiom for showing errors to user?
            // For now, display as primary text of output:
            anagrams = listOf(failure_message ?: "",
                              "Android Runtime Error:",
                              failure_error ?: "(error message not available)")
        }
        return anagrams
    }

    public fun getAnagrams(): Resultset {
        return anagrams
    }

    // See Rust lib code for `pub extern "system" fn` with name of
    // `Java_` + <this package name> + `_` + <this class name> + `_query()`
    external fun query(fd_obj: Any, query_keywords: String): Resultset

    // https://developer.android.com/training/articles/perf-jni#native-libraries
    // "Call System.loadLibrary from a static class initializer. [...]
    // If you have only one class with native methods, it makes sense
    // for the call to System.loadLibrary to be in a static
    // initializer for that class."
    //
    // However, Kotlin's equivalent to Java's static initializers
    // necessitates a `companion object` with an `init` block, which
    // gets executed prior to the first instantiation of the outer
    // class.  See also: https://youtrack.jetbrains.com/issue/KT-13486
    //
    // https://kotlinlang.org/docs/reference/object-declarations.html#semantic-difference-between-object-expressions-and-declarations
    // "a companion object is initialized when the corresponding class
    // is loaded (resolved), matching the semantics of a Java static
    // initializer."
    companion object {
        var library_loaded = false
        var failure_message: String? = null
        var failure_error: String? = null

        init {
            if (BuildConfig.DEBUG) {
                Log.d("anagrams", "ATTEMPTING to load native library") //DELETE ME
            }
            try {
                System.loadLibrary("rust")
                library_loaded = true
                if (BuildConfig.DEBUG) {
                    Log.d("anagrams", "ResultsViewModel loaded native library")
                }
            }
            catch (e: UnsatisfiedLinkError) {
                Log.e("anagrams", "ResultsViewModel native library: error=" + e)
                failure_message = "Unable to load library"
                failure_error = e.toString()
            }
            catch (e: SecurityException) {
                Log.e("anagrams", "ResultsViewModel native library: error=" + e)
                failure_message = "Security Exception"
                failure_error = e.toString()
            }
        }
    }
}
