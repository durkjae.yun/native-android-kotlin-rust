package com.example.anagrams

import android.util.Log

class ResultsActivity: SingleFragmentActivity() {

    override fun createFragment(): MainFragment {
        val query = intent.getStringExtra(SEARCH_INTENT)
        if (BuildConfig.DEBUG) {
            Log.d("anagrams", "ResultsActivity get ${SEARCH_INTENT}=$query")
        }
        return MainFragment.newInstance(query)
    }
}
