# Makefile for building native Android app

all: deps update release

ANDROID_SDK ?= /home/android-sdk
ANDROID_NDK ?= /home/android-ndk

# See Android Studio or SDK manager for highest numbered Available Package
# which should match `targetSdkVersion` in build.gradle file:
ANDROID_VERSION ?= $(shell \
	grep '^\s*targetSdkVersion' android-app/app/build.gradle | \
	sed 's/^[^0-9]*\([0-9]*\)[^0-9]*$$/\1/')

.PHONY: deps
deps: dependencies

.PHONY: dependencies
dependencies: cargo-config android-deps
	(cd android-app/ && \
	   make \
		ANDROID_SDK=${ANDROID_SDK} \
		ANDROID_NDK=${ANDROID_NDK} \
		ANDROID_VERSION=${ANDROID_VERSION} \
		linux-deps android-deps)
	(for x in android-app android-rust; do \
	   cd $$x/ && make linux-deps deps; \
	   done)

.PHONY: update
update:
	(cd android-rust/ && make update)
	(cd android-app/ && make update)

.PHONY: build
build:
	(cd android-rust/ && make build)
	(cd android-app/ && \
	   make \
		ANDROID_SDK=${ANDROID_SDK} \
		ANDROID_NDK=${ANDROID_NDK} \
		ANDROID_VERSION=${ANDROID_VERSION} \
		import build)

.PHONY: release
release: clean
	(cd android-rust/ && make release)
	(cd android-app/ && \
	   make \
		ANDROID_SDK=${ANDROID_SDK} \
		ANDROID_NDK=${ANDROID_NDK} \
		ANDROID_VERSION=${ANDROID_VERSION} \
		import release)

.PHONY: clean
clean:
	(cd android-rust/ && make clean)
	(cd android-app/ && make clean)

.PHONY: dist-clean
dist-clean: clean
	(cd android-rust/ && make dist-clean)
	(cd android-app/ && make dist-clean)
